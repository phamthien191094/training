<?php
session_start();
header("Cache-control: public");
ob_start();
include_once(dirname(__DIR__) . '/app_config.php');
$thisPageName = 'contact';
include(APP_PATH.'libs/head.php');
?>
<meta http-equiv="expires" content="86400">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/form/validationEngine.jquery.css">
</head>
<body id="contact" class="contact">
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>

  <section class="c-ms">
    <div class="container">
      <p class="c-ms__ttl-en">CONTACT</p>
      <h1 class="c-ms__ttl-jp">お問い合わせ</h1>
    </div>
  </section>
  <ul class="breadcrumb">
    <li><a href="<?php echo APP_URL ?>">ハウセット</a></li>
    <li>お問い合わせ</li>
  </ul>

  <!-- .sec01 -->
  <section class="sec01">
    <img class="sec01__icon" src="<?php echo APP_ASSETS ?>img/common/img_phone_01.svg" alt="お電話でのお問い合わせ">
    <h2 class="title01">お電話でのお問い合わせ</h2>
    <a href="tel:0120-406-111" class="sec01__phone">
        <p class="sec01__phone-txt sp">タップで電話をかける</p>
        <span class="sec01__phone-number">0120-406-111</span>
        <span class="sec01__phone-time">電話受付 00:00〜00:00 （土日祝定休）</span>
      </a>
  </section>
  <!-- /.sec01 -->

  <!-- .sec02 -->
  <section class="sec02">
    <div class="container">
      <img class="sec02__icon" src="<?php echo APP_ASSETS ?>img/common/img_mail_01.svg" alt="お問い合わせフォーム">
      <h2 class="title01">お問い合わせフォーム</h2>
      <div class="stepImg">
        <img src="<?php echo APP_ASSETS; ?>img/common/form/img_step_01.svg" width="581" height="60" alt="フォームからのお問い合わせ　Step" class="pc">
        <img src="<?php echo APP_ASSETS; ?>img/common/form/img_step_01_sp.svg" width="335" height="60" alt="フォームからのお問い合わせ　Step" class="sp">
      </div>
      <form method="post" class="contactform" id="contactform" action="confirm/?g=<?php echo time() ?>" name="contactform" onSubmit="return check()">
        <dl>
          <dt><label for="name"><span class="require">必須</span>お名前</label></dt>
          <dd>
            <p class="txt">例：山田 太郎</p>
            <input type="text" name="name" id="name" class="validate[required]">
          </dd>
        </dl>
        <dl>
          <dt><label for="furigana"><span class="require">必須</span>フリガナ</label></dt>
          <dd>
            <p class="txt">例：ヤマダ　タロウ</p>
            <input type="text" name="furigana" id="furigana" class="validate[required]">
          </dd>
        </dl>
        <dl>
          <dt><label for="zipcode"><span class="any">任意</span>郵便番号</label></dt>
          <dd>
            <p class="txt">例：1234567</p>
            <span class="txt">〒</span><input type="text" name="zipcode" id="zipcode" onKeyUp="AjaxZip3.zip2addr(this,'','address','address')" class="validate[custom[zipcode]]">
          </dd>
        </dl>
        <dl>
          <dt><label for="address"><span class="require">必須</span>ご住所</label></dt>
          <dd>
            <p class="txt">例：〇〇県〇〇市〇〇123−12</p>
            <input type="text" name="address" id="address" class="validate[required]">
          </dd>
        </dl>
        <dl>
          <dt><label for="tel"><span class="require">必須</span>電話番号</label></dt>
          <dd>
            <p class="txt">例：0120231241</p>
            <input type="tel" name="tel" id="tel" class="validate[required,custom[phone]]">
          </dd>
        </dl>
        <dl>
          <dt><label for="email"><span class="require">必須</span>E-mail</label></dt>
          <dd>
            <p class="txt">例：sample@housset.com</p>
            <input type="email" name="email" id="email" class="validate[required,custom[email]]">
          </dd>
        </dl>
        <dl>
          <dt><label for="content"><span class="require">必須</span>お問い合わせ内容</label></dt>
          <dd>
            <textarea name="content" id="content" class="validate[required]"></textarea>
          </dd>
        </dl>
        <div class="block01">
          <p class="block01__ttl">【個人情報の取扱いについて】</p>
          <p class="block01__txt01">本フォームからお客様が記入・登録された個人情報は、資料送付・電子メール送信・電話連絡などの目的で利用・保管し、第三者に開示・提供することはありません。<br>詳しくは<a href="<?php echo APP_URL ?>privacy/" target="_blank">プライバシーポリシー</a>をご覧ください。</p>
          <label class="agree"><input type="checkbox" name="check1" value="ok"><span>個人情報の取り扱いに同意する</span></label>
          <button class="btn01" id="btnConfirm"><span>入力内容を確認する</span></button>
          <input type="hidden" name="actionFlag" value="confirm">
          <p class="txt-bottom">上記フォームで送信できない場合は、必要項目をご記入の上、<a id="mailContact" href="#"></a> までメールをお送りください。</p>
        </div>
      </form>
    </div>
  </section>
  <!-- /.sec02 -->

<!-- FOOTER -->
<footer class="contact-footer">
  <p>&copy;2019 Housset Co.,Ltd.</p>
</footer>
<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/jquery.validationEngine.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/languages/jquery.validationEngine-ja.js"></script>
<script>
  $(document).ready(function(){
    $('#contactform').validationEngine({
      promptPosition: 'topLeft',
      scrollOffset: ($('.header').outerHeight() + 5),
    });
    window.onbeforeunload = function(){
      if(document.contactform.check1 && document.contactform.check1.checked) {
        $('html, body').scrollTop($('#contactform').offset().top);
      }
    };
  })
  function check(){
    if(document.contactform.check1 && !document.contactform.check1.checked){
      window.alert('「個人情報の取扱いに同意する」にチェックを入れて下さい');
      return false;
    }
  }
  $(document).ready(function() {
    if($('#mailContact').length) {
      var address = 'info' + '@' + 'housset.co.jp';
      $('#mailContact').attr('href', 'mailto:' + address).text(address);
    }
  });
</script>
</body>
</html>
