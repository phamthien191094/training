<?php
session_start();
ob_start();
include_once(dirname(__DIR__) . '/app_config.php');
if(empty($_POST['actionFlag']) && empty($_SESSION['statusFlag'])) header('location: '.APP_URL);

$gtime = time();

//always keep this
$actionFlag       = (!empty($_POST['actionFlag'])) ? htmlspecialchars($_POST['actionFlag']) : '';
$reg_url          = (!empty($_POST['url'])) ? htmlspecialchars($_POST['url']) : '';
//end always keep this

//お問い合わせフォーム内容
$reg_name         = (!empty($_POST['name'])) ? htmlspecialchars($_POST['name']) : '';
$reg_furigana     = (!empty($_POST['furigana'])) ? htmlspecialchars($_POST['furigana']) : '';
$reg_zipcode      = (!empty($_POST['zipcode'])) ? htmlspecialchars($_POST['zipcode']) : '';
$reg_address      = (!empty($_POST['address'])) ? htmlspecialchars($_POST['address']) : '';
$reg_tel          = (!empty($_POST['tel'])) ? htmlspecialchars($_POST['tel']) : '';
$reg_email        = (!empty($_POST['email'])) ? htmlspecialchars($_POST['email']) : '';
$reg_content      = (!empty($_POST['content'])) ? htmlspecialchars($_POST['content']) : '';
$br_reg_content   = nl2br($reg_content);

if($actionFlag == "confirm") {
  $thisPageName = 'contact';
  include(APP_PATH.'libs/head.php');
  $_SESSION['ses_from_step2'] = true;
  if(!isset($_SESSION['ses_gtime_step2'])) $_SESSION['ses_gtime_step2'] = $gtime;
?>
  <meta name="format-detection" content="telephone=no">
  <link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
  <!-- Anti spam part1: the contact form start -->

  <?php if(GOOGLE_RECAPTCHA_KEY_API != '' && GOOGLE_RECAPTCHA_KEY_SECRET != '') { ?>
    <script src="https://www.google.com/recaptcha/api.js?hl=ja" async defer></script>
    <script>function onSubmit(token) { document.getElementById("confirmform").submit(); }</script>
    <style>.grecaptcha-badge {display: none}</style>
  <?php } ?>

  </head>
  <body id="contact" class="confirm">
    <!-- HEADER -->
    <header class="contact-header">
      <img src="<?php echo APP_ASSETS ?>img/common/logo.svg" alt="HOUSET HOUSE ASSET">
    </header>

    <section class="c-ms02">
      <p class="c-ms02__ttl-en">CONTACT</p>
      <h1 class="c-ms02__ttl-jp">お問い合わせ</h1>
    </section>

    <section class="sec01">
      <div class="container">
        <div class="stepImg">
          <img src="<?php echo APP_ASSETS; ?>img/common/form/img_step_02.svg" width="581" height="60" alt="フォームからのお問い合わせ　Step" class="pc">
          <img src="<?php echo APP_ASSETS; ?>img/common/form/img_step_02_sp.svg" width="335" height="60" alt="フォームからのお問い合わせ　Step" class="sp">
        </div>
        <div class="sec01__txt">
          <p>以下の内容に誤りがなければ、「この内容で送信する」ボタンを押してください。</p>
        </div>
        <form method="post" class="confirmform" action="../complete/?g=<?php echo $gtime ?>" name="confirmform" id="confirmform">
          <dl>
            <dt>お名前</dt>
            <dd><?php echo $reg_name; ?></dd>
          </dl>
          <dl>
            <dt>フリガナ</dt>
            <dd><?php echo $reg_furigana; ?></dd>
          </dl>
          <?php if(!empty($reg_zipcode)) { ?>
          <dl>
            <dt>郵便番号</dt>
            <dd>〒 <?php echo $reg_zipcode; ?></dd>
          </dl>
          <?php } ?>
          <dl>
            <dt>ご住所</dt>
            <dd><?php echo $reg_address; ?></dd>
          </dl>
          <dl>
            <dt>電話番号</dt>
            <dd><?php echo $reg_tel; ?></dd>
          </dl>
          <dl>
            <dt>E-mail</dt>
            <dd><?php echo $reg_email; ?></dd>
          </dl>
          <dl>
            <dt>ご連絡事項</dt>
            <dd><?php echo $br_reg_content; ?></dd>
          </dl>

          <input type="hidden" name="name" value="<?php echo $reg_name ?>">
          <input type="hidden" name="furigana" value="<?php echo $reg_furigana ?>">
          <input type="hidden" name="zipcode" value="<?php echo $reg_zipcode ?>">
          <input type="hidden" name="address" value="<?php echo $reg_address ?>">
          <input type="hidden" name="tel" value="<?php echo $reg_tel ?>">
          <input type="hidden" name="email" value="<?php echo $reg_email ?>">
          <input type="hidden" name="content" value="<?php echo $reg_content ?>">
          <!-- always keep this -->
          <input type="hidden" name="url" value="<?php echo $reg_url ?>">
          <!-- end always keep this -->

          <div class="back">
            <a href="javascript:history.back()">入力内容を修正する</a>
          </div>
          <?php if(GOOGLE_RECAPTCHA_KEY_API != '') { ?>
            <button name="actionFlag" value="send" class="g-recaptcha" data-size="invisible" data-sitekey="<?php echo GOOGLE_RECAPTCHA_KEY_API ?>" data-callback="onSubmit"><span>この内容で送信する</span></button>
          <?php } else { ?>
            <button id="btnSend" class="btn02"><span>この内容で送信する</span></button>
          <?php } ?>
          <input type="hidden" name="actionFlag" value="send">

          <p class="txt-bottom">上記フォームで送信できない場合は、必要項目をご記入の上、<a id="mailContact" href="#"></a> までメールをお送りください。</p>
        </form>
      </div>
    </section>

    <!-- FOOTER -->
    <footer class="contact-footer">
      <p>&copy;2019 Housset Co.,Ltd.</p>
    </footer>
    <script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>
    <script>
      $(document).ready(function() {
        var address = 'info' + '@' + 'housset.co.jp';
        $("#mailContact").attr("href", "mailto:" + address).text(address);
        $('#confirmform').on('click','#btnSend',function(e){
          e.preventDefault();
          $(this).html('<span>送信中...</span>').prop('disabled',true).addClass('disabled');
          $('#confirmform').submit();
        })
      });
    </script>
  </body>
  </html>
<?php } ?>