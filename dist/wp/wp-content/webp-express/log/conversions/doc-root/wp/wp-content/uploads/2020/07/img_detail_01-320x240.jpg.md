WebP Express 0.17.4. Conversion triggered using bulk conversion, 2020-07-30 16:40:35

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.2.12
- Server software: nginx/1.15.6

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg
- destination: [doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 70
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg
- destination: [doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 70
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- default-quality: 75
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "alivevn" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.0)
- /usr/local/bin/cwebp: (version: 1.0.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.0
Quality: 70. 
Consider setting quality to "auto" instead. It is generally a better idea
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 70 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossy.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.0
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 70 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossy.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossy.webp'
File:      [doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg
Dimension: 320 x 240
Output:    10982 bytes Y-U-V-All-PSNR 37.65 43.77 44.51   38.95 dB
           (1.14 bpp)
block count:  intra4:        270  (90.00%)
              intra16:        30  (10.00%)
              skipped:         0  (0.00%)
bytes used:  header:            113  (1.0%)
             mode-partition:   1195  (10.9%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    8332 |      38 |      64 |      38 |    8472  (77.1%)
 intra16-coeffs:  |     206 |      17 |      32 |       5 |     260  (2.4%)
  chroma coeffs:  |     854 |       7 |      29 |      24 |     914  (8.3%)
    macroblocks:  |      91%|       2%|       4%|       3%|     300
      quantizer:  |      29 |      22 |      16 |      16 |
   filter level:  |      25 |       5 |       3 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    9392 |      62 |     125 |      67 |    9646  (87.8%)

Success
Reduction: 43% (went from 19 kb to 11 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "alivevn" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.0)
- /usr/local/bin/cwebp: (version: 1.0.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.0
Trying to convert by executing the following command:
nice cwebp -metadata none -q 70 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossless.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.0
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 70 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossless.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg.webp.lossless.webp'
File:      [doc-root]/wp/wp-content/uploads/2020/07/img_detail_01-320x240.jpg
Dimension: 320 x 240
Output:    55096 bytes (5.74 bpp)
Lossless-ARGB compressed size: 55096 bytes
  * Header size: 2237 bytes, image data size: 52833
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=10

Success
Reduction: -185% (went from 19 kb to 54 kb)

Picking lossy
cwebp succeeded :)

Converted image in 440 ms, reducing file size with 43% (went from 19 kb to 11 kb)
