WebP Express 0.17.4. Conversion triggered using bulk conversion, 2020-07-30 13:07:43

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.2.12
- Server software: nginx/1.15.6

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg
- destination: [doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 70
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg
- destination: [doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 70
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- default-quality: 75
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "alivevn" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.0)
- /usr/local/bin/cwebp: (version: 1.0.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.0
Quality: 70. 
Consider setting quality to "auto" instead. It is generally a better idea
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 70 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossy.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.0
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 70 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossy.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossy.webp'
File:      [doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg
Dimension: 416 x 274
Output:    9836 bytes Y-U-V-All-PSNR 39.13 40.72 40.98   39.63 dB
           (0.69 bpp)
block count:  intra4:        383  (81.84%)
              intra16:        85  (18.16%)
              skipped:         3  (0.64%)
bytes used:  header:             82  (0.8%)
             mode-partition:   1610  (16.4%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    5222 |     145 |     134 |      56 |    5557  (56.5%)
 intra16-coeffs:  |     194 |      77 |      36 |      69 |     376  (3.8%)
  chroma coeffs:  |    1938 |      86 |      89 |      69 |    2182  (22.2%)
    macroblocks:  |      77%|       7%|       6%|       9%|     468
      quantizer:  |      32 |      23 |      16 |      16 |
   filter level:  |      27 |       5 |      15 |       7 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    7354 |     308 |     259 |     194 |    8115  (82.5%)

Success
Reduction: 93% (went from 136 kb to 10 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.0*
- Executing: [doc-root]/wp/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "alivevn" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.0)
- /usr/local/bin/cwebp: (version: 1.0.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.0
Trying to convert by executing the following command:
nice cwebp -metadata none -q 70 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossless.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.0
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 70 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg' -o '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossless.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp/wp-content/webp-express/webp-images/doc-root/wp/wp-content/uploads/2020/05/img_blog_06.jpg.webp.lossless.webp'
File:      [doc-root]/wp/wp-content/uploads/2020/05/img_blog_06.jpg
Dimension: 416 x 274
Output:    89068 bytes (6.25 bpp)
Lossless-ARGB compressed size: 89068 bytes
  * Header size: 3093 bytes, image data size: 85950
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=10

Success
Reduction: 36% (went from 136 kb to 87 kb)

Picking lossy
cwebp succeeded :)

Converted image in 617 ms, reducing file size with 93% (went from 136 kb to 10 kb)
