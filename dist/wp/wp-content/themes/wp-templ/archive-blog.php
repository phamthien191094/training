<?php
$thisPageName = 'blog';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/blog.min.css">
</head>
<body id="blog" class='blog'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
  <main>
    <section class="c-ms">
      <div class="container">
        <p class="c-ms__ttl-en">BLOG</p>
        <h1 class="c-ms__ttl-jp">ハウセットブログ</h1>
      </div>
    </section>
    <ul class="breadcrumb">
      <li><a href="<?php echo APP_URL ?>">ハウセット</a></li>
      <li>ブログ</li>
    </ul>
    <div class="container">
      <div class="content">
        <section class="main">
          <?php
          parse_str( $query_string, $param );
          $post_show = 15;
          $param += array(
            'post_type'                => 'blog',
            'order'                    => 'DESC',
            'post_status'              => 'publish',
            'posts_per_page'           => $post_show,
            'paged'                    => $paged
          );
          $wp_query = new WP_Query($param);
          if($wp_query->have_posts()):
          ?>
          <ul class="list01">
            <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <li>
              <a href="<?php the_permalink(); ?>">
                <?php
                $days = 20;
                $today = date('U'); $entry=get_the_time('U');
                $diff1 = date('U',($today - $entry))/86400;
                if ($days > $diff1) {
                  echo '<span class="list01__new">NEW</span>';
                }
                // image
                $image = wp_get_attachment_image_src(get_field('main-img'),'medium');
                if ($image) :
                  $url_image = $image[0];
                else :
                  $url_image = APP_ASSETS."img/common/other/img_nophoto.jpg";
                endif;
                ?>
                <div class="list01__image" style="background-image: url(<?php echo $url_image; ?>);"></div>
                <div class="list01__info">
                  <span class="list01__date"><?php the_time('Y.m.d'); ?></span>
                  <?php
                  $terms = get_the_terms($post->ID, 'blogcat');
                  if($terms) {
                    foreach($terms as $term) {
                  ?>
                       <span class="list01__cat"><?php echo $term->name; ?></span>
                  <?php
                    }
                  }
                  ?>
                  <h2 class="list01__text"><?php the_title(); ?></h2>
                </div>
              </a>
            </li>
            <?php endwhile; ?>
          </ul>
          <div class="pagenavi-sp sp">
            <?php
            if(function_exists('wp_pagenavi')) {
              previous_posts_link( __( '<span class="prev"></span>', '' ) );
            }
            $post_total = $wp_query->found_posts;
            $page_total = CEIL($post_total / $post_show);
            $page_current = ($paged == 0) ? 1 : $paged;
            global $wp;
            $current_url = home_url( $wp->request );
            $pos = strpos($current_url , '/page');
            $url = ($pos) ? substr($current_url,0,$pos) : $current_url;
            if($post_total > $post_show) {
            ?>
            <div class="pagenavi-select">
              <form action="" method="get">
                <select size="1" onchange="document.location.href = this.options[this.selectedIndex].value;">
                  <option value="<?php echo $page_current."/".$page_total; ?>"><?php echo $page_current."/".$page_total; ?></option>
                  <?php
                  for ($i = 1; $i <= $page_total; $i++) {
                    $url_i = ($i == 1) ? $url."/" : $url."/page/".$i."/";
                  ?>
                  <option value="<?php echo $url_i; ?>"><?php echo $i; ?></option>
                  <?php
                  }
                  ?>
                </select>
              </form>
            </div>
           <?php
            }
            if(function_exists('wp_pagenavi')) {
              next_posts_link( __( '<span class="next"></span>', '' ) );
            }
            ?>
          </div>
          <?php endif; wp_reset_postdata(); ?>
        </section>
        <?php include(APP_PATH.'libs/sidebar.php'); ?>
      </div>
      <div class="pagenavi-pc pc">
        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
      </div>
    </div>
  </main>
  <!-- main -->

<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>