<?php
$thisPageName = 'single-blog';
$title_sg = get_the_title();
$desPage = mb_substr(preg_replace('/\r\n|\n|\r/','',strip_tags($post->post_content)),0,120);
$image = wp_get_attachment_image_src(get_field('main-img'),'full');
$ogimg = $image[0];
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/blog-detail.min.css">
</head>
<body id="blog" class="blog-detail">
  <!-- HEADER -->
  <?php include(APP_PATH.'libs/header.php'); ?>
  <main>
    <section class="c-ms02">
      <p class="c-ms02__ttl-en">BLOG</p>
      <h1 class="c-ms02__ttl-jp">ハウセットブログ</h1>
    </section>
    <ul class="breadcrumb">
      <li><a href="<?php echo APP_URL ?>">ハウセット</a></li>
      <li><a href="<?php echo APP_URL ?>blog/">ブログ</a></li>
      <li><?php the_title(); ?></li>
    </ul>
    <div class="container">
      <div class="content">
        <section class="main">
          <div class="heading">
            <span class="date"><?php the_time('Y.m.d'); ?></span>
            <?php
            $terms = get_the_terms($post->ID, 'blogcat');
            if($terms) {
              foreach($terms as $term) {
            ?>
              <span class="cat"><?php echo $termname = $term->name; ?></span>
            <?php
              }
            }
            ?>
            <h2 class="title"><?php the_title(); ?></h2>
          </div>
          <div class="detail">
            <?php
            if($image) {
            ?>
            <img src="<?php echo $image[0]?>" width="<?php echo $image[1]?>" height="<?php echo $image[2]?>" alt="<?php echo the_title(); ?>"/>
            <?php
            }
            if (have_posts()) : while (have_posts()) : the_post();
            ?>
              <?php the_content();?>
            <?php endwhile; endif; ?>
          </div>
          <div class="sns">
            <p class="sns__ttl">SHARE ON :</p>
            <ul class="sns__list">
              <li>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                  <img src="<?php echo APP_ASSETS ?>img/blog/detail/ico_facebook_01.svg" alt="facebook">
                </a>
              </li>
              <li>
                <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank">
                  <img src="<?php echo APP_ASSETS ?>img/blog/detail/ico_instagram_01.svg" alt="instagram">
                </a>
              </li>
              <li>
                <a href="https://lineit.line.me/share/ui?url=<?php the_permalink(); ?>" target="_blank">
                  <img src="<?php echo APP_ASSETS ?>img/blog/detail/ico_line_01.svg" alt="line">
                </a>
              </li>
            </ul>
          </div>
        </section>
        <section class="sec01 sp">
          <?php include(APP_PATH.'libs/list01_blog.php'); ?>
          <a href="<?php echo APP_URL ?>blog/" class="sec01__btn-sp">一覧へ戻る</a>
        </section>
        <?php include(APP_PATH.'libs/sidebar.php'); ?>
      </div>
    </div>
    <section class="sec01 pc">
      <?php include(APP_PATH.'libs/list01_blog.php'); ?>
      <a href="<?php echo APP_URL ?>/blog/" class="sec01__btn">一覧へ戻る</a>
    </section>
  </main>
  <!-- HEADER -->
  <?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>