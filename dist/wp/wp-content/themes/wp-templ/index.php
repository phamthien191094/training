<?php
$thisPageName = 'top';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/lib/slick.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/top.min.css">
</head>
<body id="top" class='top'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>

    <!-- .sec01 -->
    <section class="sec01">
      <div class="sec01__title">
        <p class="sec01__title__txt">
          <img src="<?php echo APP_ASSETS ?>img/top/ttl_top_01.svg" width="239" height="64" alt="Design your style">
        </p>
        <h1 class="sec01__title__main">
          <span>理想の住まい</span>
          <span>を<br>理想の価格で手にいれる</span>
          <span>デザイン分譲住宅・デザイナーズ分譲住宅というカタチ</span>
        </h1>
      </div>
      <div class="slider">
        <div class="slider__item">
          <div class="slider__image">
            <picture>
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_01_sp.jpg" media="(max-width: 767px)">
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_01.jpg">
              <img src="<?php echo APP_ASSETS ?>img/top/ms_top_01.jpg" alt="">
            </picture>
          </div>
        </div>
        <div class="slider__item">
          <div class="slider__image">
            <picture>
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_02_sp.jpg" media="(max-width: 767px)">
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_02.jpg">
              <img src="<?php echo APP_ASSETS ?>img/top/ms_top_02.jpg" alt="">
            </picture>
          </div>
        </div>
        <div class="slider__item">
          <div class="slider__image">
            <picture>
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_03_sp.jpg" media="(max-width: 767px)">
              <source srcset="<?php echo APP_ASSETS ?>img/top/ms_top_03.jpg">
              <img src="<?php echo APP_ASSETS ?>img/top/ms_top_03.jpg" alt="">
            </picture>
          </div>
        </div>
      </div>
      <div class="sec01__scroll-down">
        <a href="#sec02"><span></span>SCROLL DOWN</a>
      </div>
    </section>
    <!-- /.sec01 -->

    <!-- .sec02 -->
    <section class="sec02" id="sec02">
      <div class="title">
        <p class="title__main">SALES</p>
        <h2 class="title__text">販売中の分譲住宅</h2>
      </div>
      <ul class="list01">
        <li class="list01__full-sp">
          <a href="a">
             <div class="list01__image">
               <picture>
                 <source srcset="<?php echo APP_ASSETS ?>img/top/img_sales_01_sp.jpg" media="(max-width: 767px)">
                 <source srcset="<?php echo APP_ASSETS ?>img/top/img_sales_01.jpg">
                 <img src="<?php echo APP_ASSETS ?>img/top/img_sales_01.jpg" alt="">
               </picture>
             </div>
             <p class="list01__box">大型分譲住宅<br>トワイエ浦安特設サイトへ.</p>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list01__image">
               <img src="<?php echo APP_ASSETS ?>img/top/img_sales_02.jpg" alt="">
             </div>
             <div class="list01__info">
               <h3 class="list01__title">ファミーナ船堀III</h3>
               <p class="list01__text">住所：東京都江戸川区中央3丁目東京都江戸川区中央3丁目</p>
             </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list01__image">
               <img src="<?php echo APP_ASSETS ?>img/top/img_sales_03.jpg" alt="">
             </div>
             <div class="list01__info">
               <h3 class="list01__title">ファミーナ船堀III</h3>
               <p class="list01__text">住所：東京都江戸川区松江二丁目</p>
             </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list01__image">
               <img src="<?php echo APP_ASSETS ?>img/top/img_sales_04.jpg" alt="">
             </div>
             <div class="list01__info">
               <h3 class="list01__title">ファミーナ船堀III</h3>
               <p class="list01__text">住所：東京都江戸川区中央3丁目東京都江戸川区中央3丁目</p>
             </div>
          </a>
        </li>
        <li class="sp">
          <a href="a">
            <div class="list01__image">
               <img src="<?php echo APP_ASSETS ?>img/top/img_sales_03.jpg" alt="">
             </div>
             <div class="list01__info">
               <h3 class="list01__title">ファミーナ船堀III</h3>
               <p class="list01__text">住所：東京都江戸川区松江二丁目</p>
             </div>
          </a>
        </li>
      </ul>
      <a href="<?php echo APP_URL ?>sale/" class="btn01"><span>販売中の分譲住宅を<br class="sp">もっと見る</span></a>
    </section>
    <!-- .sec02 -->

    <!-- .sec03 -->
    <section class="sec03">
      <div class="sec03__inner">
        <div class="sec03__image">
          <picture>
            <source srcset="<?php echo APP_ASSETS ?>img/top/img_about_01_sp.png" media="(max-width: 767px)">
            <source srcset="<?php echo APP_ASSETS ?>img/top/img_about_01.png">
            <img src="<?php echo APP_ASSETS ?>img/top/img_about_01.png" alt="">
          </picture>
        </div>
        <div class="sec03__content">
          <div class="title02">
            <p class="title02__text">HOUSE <span class="title02__plus"></span> ASSET</p>
            <h2 class="title02__main">ABOUT US</h2>
          </div>
          <div class="text">
            <p>高いデザイン性<br>暮らしやすい間取り<br>最新で使いやすい住宅設備<br>高い断熱性と耐震性</p>
            <p>ハウセットがご提供する分譲住宅は<br>ただデザインに優れているだけではなく<br>そこに住まう方が楽しく快適に暮らし<br>安心して暮らすことができる性能です</p>
            <p>私たちのデザイン分譲住宅・<br class="sp">デザイナーズ分譲住宅は<br>これまでの分譲住宅になかった<br class="sp">新しい住まいの形です</p>
          </div>
          <a href="<?php echo APP_URL ?>about-us/" class="btn01 btn01--color1"><span>ハウセットについて</span></a>
        </div>
      </div>
    </section>
    <!-- /.sec03 -->

    <!-- .sec04 -->
    <section class="sec04">
      <div class="title">
        <p class="title__main">WORKS</p>
        <h2 class="title__text">施工・販売実績</h2>
      </div>
      <ul class="list02">
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_01.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワーク</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_02.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワークスタイトルワークスタイトル</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_03.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワークスタイトルワークスタイ</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_04.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワークスタイトルワークスタイ</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_05.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワークスタイトルワークスタイ</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
        <li>
          <a href="a">
            <div class="list02__image">
              <img src="<?php echo APP_ASSETS ?>img/top/img_works_06.jpg" alt="">
            </div>
            <div class="list02__info">
              <p class="list02__title">ワークスタイトルワークスタイトルワークスタイトルワークスタイトルワークスタイ</p>
              <p class="list02__text">○市　○様邸</p>
            </div>
          </a>
        </li>
      </ul>
      <a href="<?php echo APP_URL ?>works/" class="btn01"><span>施工・販売実績をもっと見る</span></a>
    </section>
    <!-- /.sec04 -->

    <!-- .sec05 -->
    <section class="sec05">
      <div class="sec05__mv">
        <img src="<?php echo APP_ASSETS ?>img/top/ms_blog_01.jpg" alt="">
      </div>
      <div class="sec05__inner">
        <div class="title">
          <p class="title__main">BLOG</p>
          <h2 class="title__text">ハウセットブログ</h2>
        </div>
        <?php
        $param = array(
          'post_type'                => 'blog',
          'order'                    => 'DESC',
          'post_status'              => 'publish',
          'posts_per_page'           => '6'
        );
        $wp_query = new WP_Query($param);
        if($wp_query->have_posts()):
        ?>
          <ul class="list03">
          <?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <li>
              <a href="<?php the_permalink(); ?>">
                <?php
                $days = 20;
                $today = date('U'); $entry=get_the_time('U');
                $diff1 = date('U',($today - $entry))/86400;
                if ($days > $diff1) {
                  echo '<span class="list03__new">NEW</span>';
                }

                //image
                $image = wp_get_attachment_image_src(get_field('main-img'),'medium');
                if ($image) :
                  $url_image = $image[0];
                else :
                  $url_image = APP_ASSETS."img/common/other/img_nophoto.jpg";
                endif;
                ?>
                <div class="list03__image" style="background-image: url(<?php echo $url_image; ?>);"></div>
                <div class="list03__info">
                  <span class="list03__date"><?php the_time('Y.m.d'); ?></span>
                  <?php
                  $terms = get_the_terms($post->ID, 'blogcat');
                  if($terms) {
                    foreach($terms as $term) {
                  ?>
                       <span class="list03__cat"><?php echo $term->name; ?></span>
                  <?php
                    }
                  }
                  ?>
                  <p class="list03__text"><?php the_title(); ?></p>
                </div>
              </a>
            </li>
          <?php endwhile; ?>
          </ul>
        <?php endif; wp_reset_postdata();?>
        <a href="<?php echo APP_URL ?>blog/" class="btn01"><span>ブログ一覧</span></a>
      </div>
    </section>
    <!-- /.sec05 -->

    <!-- .sec06 -->
    <section class="sec06">
      <ul class="list04">
        <li>
          <div class="list04__title">
            <p class="list04__title__main">COMPANY</p>
            <h2 class="list04__title__text">会社情報</h2>
          </div>
          <div class="list04__image">
            <img src="<?php echo APP_ASSETS ?>img/top/img_company.png" alt="">
          </div>
          <p class="list04__text">テキストが入りますテキストが<br class="pc">入りますテキストが入りますテ<br class="pc">キストが入ります。</p>
          <a href="<?php echo APP_URL ?>company/" class="btn01"><span>詳しく見る</span></a>
        </li>
        <li>
          <div class="list04__title">
            <p class="list04__title__main">HOUSET <br class="sp">CLUB</p>
            <h2 class="list04__title__text">ハウセットクラブ</h2>
          </div>
          <div class="list04__image">
            <img src="<?php echo APP_ASSETS ?>img/top/img_houset-club.png" alt="">
          </div>
          <p class="list04__text">未公開物件など<br class="pc">お得な情報を<br class="sp">お届けします<br class="pc">ハウセットクラブ</p>
          <a href="https://housset.co.jp/mail/club/example.html" class="btn01">
            <span class="pc">ハウセットクラブについて</span>
            <span class="sp">詳しく見る</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sec06 -->

    <!-- .sec07 -->
    <section class="sec07">
      <div class="sec07__mv">
        <picture>
          <source srcset="<?php echo APP_ASSETS ?>img/top/ms_design_01_sp.jpg" media="(max-width: 767px)">
          <source srcset="<?php echo APP_ASSETS ?>img/top/ms_design_01.jpg">
          <img src="<?php echo APP_ASSETS ?>img/top/ms_design_01.jpg" alt="">
        </picture>
      </div>
      <div class="sec07__inner">
        <div class="block01">
          <div class="block01__info">
            <h2 class="block01__title">ハウセットの<br>デザイン分譲住宅</h2>
            <div class="block01__text">
              <p>分譲住宅や建売ってつまらない。</p>
              <p>そう感じている方もいらっしゃるかと思います。ハウセットはそんな普通の分譲住宅では満足できない方に向けた「デザイン分譲住宅」をご提供しています。</p>
              <p>デザイン性が高いだけではなく、そこに住まう人たちのことを想像しながら設計を行う暮らしやすい間取り。</p>
              <p>累計1,000棟を超える私たちだからこそできる高いデザイン性と暮らしやすい間取りに少しの遊び心を加えたデザイン分譲住宅。</p>
              <p>一般的な分譲住宅や建売では手に入れることができない豊かなライフスタイルについて。</p>
            </div>
            <a href="<?php echo APP_URL ?>about-us/design/" class="btn01 pc"><span>デザイン性と間取りについて</span></a>
          </div>
          <div class="block01__image">
            <picture>
              <source srcset="<?php echo APP_ASSETS ?>img/top/img_design_01_sp.jpg" media="(max-width: 767px)">
              <source srcset="<?php echo APP_ASSETS ?>img/top/img_design_01.jpg">
              <img src="<?php echo APP_ASSETS ?>img/top/img_design_01.jpg" alt="">
            </picture>
          </div>
        </div>
      </div>
    </section>
    <!-- /.sec07 -->

  </main>
</div> <!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS ?>js/lib/slick.min.js"></script>
<script src="<?php echo APP_ASSETS ?>js/page/top.min.js"></script>
</body>
</html>