<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'training' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'alive1612' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8K|dcxJ-UF/Kh{H%BwQ{7s05:<^iS*2qAvUA4*{)cLuIqH)4h32<YbW9el$X`%MP');
define('SECURE_AUTH_KEY',  ';wR6.B5gC,OSkt|JZYC@`b5iQjz:c/vG6M?PE.d}i&M@}gfbalsdeuW#57WERi6s');
define('LOGGED_IN_KEY',    'mPdyu<T~^!~-@m~Wb>R<hU}r:l+[])]2+@M+us,dJN$$DGJ8-O.P7fmn6WjgjaXK');
define('NONCE_KEY',        ' 3%:|EWI,nl6=T1zTjKujF{Z8=lx`]Yx+rN0--0<JDF8}]9.*J:$dW3+<{-*kU?-');
define('AUTH_SALT',        '7JGgr!0+AXY!*[M%nFH3lQLb=7$B|nQ3/W+7Z!ZaX^bOAa?E-0*W_]x#c]3 1:BG');
define('SECURE_AUTH_SALT', ';|,B?R@))nR)Z7&B,:qF8mru-LB?WoG#h)<oGTc;H+?OeGjiB!|7D;?<93}~yaK?');
define('LOGGED_IN_SALT',   'otJl{9PPsoi>Xg^lsh5r@2f4#o!r`3L9{F2R&-UBq/OXO|R^GNcJ5]QaQYZF??C8');
define('NONCE_SALT',       '`+Gk+!e]D)W$I^:ORdd=r/8AJwx~7&y-s;|-:K@~+O+5ud2nEN#<ZM%mgEN>^,@-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'training_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define( 'WP_AUTO_UPDATE_CORE', false ); //turnoff wp auto update
add_filter( 'auto_update_plugin', '__return_false' ); //turnoff plugin auto update
add_filter( 'auto_update_theme', '__return_false' ); //turnoff theme auto update
