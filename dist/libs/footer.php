<section class="fContact">
  <div class="fContact__title">
    <p class="fContact__ttl1"><img src="<?php echo APP_ASSETS ?>img/common/ttl_contact.svg" width="206" height="64" alt="Feel free to contact us"></p>
    <p class="fContact__ttl2"><img src="<?php echo APP_ASSETS ?>img/common/img_phone_01.svg" width="79" height="77" alt="">CONTACT</p>
    <h2 class="fContact__ttl3">ハウセットへのお問い合わせ</h2>
  </div>
  <ul class="fContact__btnGroup">
    <li>
      <a href="tel:0120-406-111" class="fContact__phone">
        <p class="fContact__phone-txt sp">電話でお問い合えあせ</p>
        <span class="fContact__phone-number">0120-406-111</span>
        <span class="fContact__phone-time">電話受付 00:00〜00:00 （土日祝定休）</span>
      </a>
    </li>
    <li>
      <a href="https://housset.co.jp/mail/club/example.html" class="c-btn c-btn--card c-btn--arrow">
        <span>ハウセットクラブ</span>
      </a>
    </li>
    <li>
      <a href="<?php echo APP_URL ?>contact/" class="c-btn c-btn--mail c-btn--arrow">
        <span>お問い合わせ</span>
      </a>
    </li>
  </ul>
</section>

<footer class="footer">
  <div class="footer__inner">
    <nav class="footer__nav">
      <ul>
        <li><a href="<?php echo APP_URL ?>">ホーム</a></li>
        <li>
          <a href="<?php echo APP_URL ?>about-us/" class="js-slide">ハウセットの建てる家</a>
          <ul class="footer__subNav">
            <li><a href="<?php echo APP_URL ?>about-us/quality/">高品質なヒミツ</a></li>
            <li><a href="<?php echo APP_URL ?>about-us/design/">デザイン性と間取りのヒミツ</a></li>
            <li><a href="<?php echo APP_URL ?>about-us/technology/">耐震・断熱・構造のヒミツ</a></li>
            <li><a href="<?php echo APP_URL ?>sale/">アフターメンテナンスのヒミツ</a></li>
          </ul>
        </li>
        <li><a href="a">資金計画についての考え方</a></li>
        <li><a href="a">販売中の物件情報</a></li>
      </ul>
      <ul>
        <li>
          <a href="<?php echo APP_URL ?>company/" class="js-slide">会社情報</a>
          <ul class="footer__subNav">
            <li><a href="a">スタッフ紹介</a></li>
            <li><a href="a">代表挨拶・経営理念</a></li>
            <li><a href="<?php echo APP_URL ?>company/outline/">会社概要・アクセスマップ</a></li>
          </ul>
        </li>
        <li><a href="<?php echo APP_URL ?>blog/">ブログ</a></li>
        <li><a href="https://www.housset.co.jp/reform-shop/">リフォームについて</a></li>
        <li><a href="a">販売 (施工) 実績</a></li>
        <li><a href="https://housset.co.jp/voice/index.html">お客様の声</a></li>
      </ul>
      <ul>
        <li><a href="https://housset.co.jp/mail/club/example.html">ハウセットクラブ申し込みフォーム</a></li>
        <li><a href="<?php echo APP_URL ?>contact/">お問い合わせ</a></li>
        <li class="footer__nav--itemSP"><a href="a">サイトマップ</a></li>
        <li class="footer__nav--itemSP"><a href="<?php echo APP_URL ?>privacy/">プライバシーポリシー</a></li>
      </ul>
    </nav>
    <div class="footer__info">
      <div class="footer__logo">
        <a href="<?php echo APP_URL ?>">
          <img src="<?php echo APP_ASSETS ?>img/common/logo_footer.svg" alt="HOSET HOUSE ASSET">
        </a>
      </div>
      <p class="footer__address">〒130-0002<br>東京都墨田区業平4-9-3 押上駅前ビルディング<br class="sp">
      (ハウセット本社ビル) <a href="a" target="_blank">Map</a></p>
      <p class="footer__phone">TEL：<a href="tel:03-3621-9795">03-3621-9795</a>（建設部:<a href="tel:03-6284-1701">03-6284-1701</a>）<br>FAX：03-3621-9796（建設部:03-6284-1702）</p>
      <div class="footer__sns">
        <p>Follow us</p>
        <ul>
          <li>
            <a href="https://www.instagram.com/house.asset.housset/" target="_blank">
              <img src="<?php echo APP_ASSETS ?>img/common/icon/ico_instagram.svg" alt="instagram">
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/house.asset.housset/" target="_blank">
              <img src="<?php echo APP_ASSETS ?>img/common/icon/ico_facebook.svg" alt="facebook">
            </a>
          </li>
        </ul>
      </div>
      <p class="footer__copyright">&copy;2019 Houset Co.,Ltd.</p>
    </div>
  </div>
  <div class="footer__scrolltop">
    <svg xmlns="http://www.w3.org/2000/svg" width="44.344" height="44.344" viewBox="0 0 44.344 44.344">
      <g id="Group_40161" data-name="Group 40161" transform="translate(-4497.328 -8966.328)">
        <circle id="Ellipse_161" data-name="Ellipse 161" cx="22.172" cy="22.172" r="22.172" transform="translate(4497.328 8966.328)" fill="#fff"/>
        <path id="Path_336656" data-name="Path 336656" d="M3.69,14.537c1.594,1.847,1.884,1.733,3.279-.938a24.647,24.647,0,0,0,2.281-2.816c.6-1.108.143-2.263-.324-2.276-.81-.024-1.995,2.026-2.375,2.737a3.73,3.73,0,0,1-.892,1.285c.279-2.46.313-4.438.842-7.8.423-2.688.782-3.1.647-3.939C6.98-.17,6.306-.353,6.061.79,4.683,7.219,5.034,8.448,4.453,12.715A22.811,22.811,0,0,1,1.519,8.2C.955,7.053,1.014,7.032.782,6.848c-.557-.331-1,.871-.668,1.831A23.869,23.869,0,0,0,3.69,14.537Z" transform="translate(4526.141 8995.786) rotate(171)" stroke="#fff" stroke-width="0.25"/>
      </g>
    </svg>
  </div>
</footer>

<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>