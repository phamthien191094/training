<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->

<header class="header">
  <div class="header__logo">
    <a href="<?php echo APP_URL ?>">
      <img src="<?php echo APP_ASSETS ?>img/common/logo.svg" alt="HOUSET HOUSE ASSET" class="header__logo--close">
      <img src="<?php echo APP_ASSETS ?>img/common/logo_open_menu_sp.svg" alt="HOUSET HOUSE ASSET" class="header__logo--open">
    </a>
  </div>
  <nav class="header__nav pc">
     <div class="header__menu">
       <ul>
         <li><a href="<?php echo APP_URL ?>about-us/">ハウセットの建てる家</a></li>
         <li class="disable"><a href="a">販売(施工)実績</a></li>
         <li><a href="https://housset.co.jp/voice/index.html">お客様の声</a></li>
         <li><a href="https://www.housset.co.jp/reform-shop/">リフォーム</a></li>
         <li><a href="<?php echo APP_URL ?>sale/">販売中の物件情報</a></li>
         <li><a href="<?php echo APP_URL ?>company/">会社概要</a></li>
       </ul>
     </div>
     <div class="header__btnGroup">
       <a href="<?php echo APP_URL ?>houset-club/" class="c-btn c-btn--card">
         <span>ハウセットクラブ</span>
       </a>
       <a href="<?php echo APP_URL ?>contact/" class="c-btn c-btn--mail header__btnBottom">
         <span>お問い合わせ</span>
       </a>
     </div>
  </nav>
  <div class="header__btnSp sp">
    <a href="a" class="c-btn c-btn--card">
      <span>ハウセットクラブ</span>
    </a>
  </div>
  <div class="header__open sp">
    <div class="header__open-icon">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
</header>
<div class="menu-sp">
  <ul class="menu-sp__nav">
    <li><a href="<?php echo APP_URL ?>">ホーム</a></li>
    <li>
      <a href="<?php echo APP_URL ?>about-us/" class="js-slide">ハウセットの建てる家</a>
      <ul class="menu-sp__subNav">
        <li><a href="<?php echo APP_URL ?>about-us/quality/">高品質なヒミツ</a></li>
        <li><a href="<?php echo APP_URL ?>about-us/design/">デザイン性と間取りのヒミツ</a></li>
        <li><a href="<?php echo APP_URL ?>about-us/technology/">耐震・断熱・構造のヒミツ</a></li>
        <li><a href="a">アフターメンテナンスのヒミツ</a></li>
      </ul>
    </li>
    <li><a href="a">資金計画についての考え方</a></li>
    <li><a href="a">販売中の物件情報</a></li>
    <li><a href="a">販売(施工)実績</a></li>
    <li>
      <a href="<?php echo APP_URL ?>company/" class="js-slide">会社情報</a>
      <ul class="menu-sp__subNav">
        <li><a href="a">スタッフ紹介</a></li>
        <li><a href="a">代表挨拶・経営理念</a></li>
        <li><a href="a">会社概要・アクセスマップ</a></li>
      </ul>
    </li>
    <li><a href="<?php echo APP_URL ?>blog/">ブログ</a></li>
    <li><a href="https://www.housset.co.jp/reform-shop/">リフォームについて</a></li>
    <li><a href="https://housset.co.jp/voice/index.html">お客様の声</a></li>
    <li><a href="https://housset.co.jp/mail/club/example.html">ハウセットクラブ申し込みフォーム</a></li>
    <li><a href="<?php echo APP_URL ?>/contact/">お問い合わせ</a></li>
    <li>
      <ul class="menu-sp__subNav02">
        <li><a href="a">サイトマップ</a></li>
        <li><a href="a">プライバシーポリシー</a></li>
      </ul>
    </li>
  </ul>
  <div class="menu-sp__title">
    <p class="menu-sp__ttl1"><img src="<?php echo APP_ASSETS ?>img/common/img_phone_02.svg" width="46" height="45" alt="">CONTACT</p>
    <p class="menu-sp__ttl2">ハウセットへのお問い合わせ</p>
  </div>
  <ul class="menu-sp__btn">
    <li>
      <a href="tel:0120-406-111" class="menu-sp__phone">
        <p class="menu-sp__phone-txt">電話でお問い合えあせ</p>
        <p class="menu-sp__phone-number">0120-406-111</p>
        <p class="menu-sp__phone-time">電話受付 00:00〜00:00 (土日祝定休）</p>
      </a>
    </li>
    <li>
      <a href="a" class="c-btn c-btn--card c-btn--arrow">
        <span>ハウセットクラブ</span>
      </a>
    </li>
    <li>
        <a href="<?php echo APP_URL ?>contact/" class="c-btn c-btn--mail c-btn--arrow">
          <span>お問い合わせ</span>
        </a>
    </li>
  </ul>
  <div class="menu-sp__close">
    <p>CLOSE</p>
  </div>
</div>