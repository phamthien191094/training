<?php
$pagename = str_replace(array('/', '.php'), '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$pagename = $pagename ? $pagename : 'default';
$pagename = (isset($thisPageName)) ? $thisPageName : $pagename;
switch ($pagename) {
  case 'top':
    if(empty($titlepage)) $titlepage = '墨田区・江戸川区・江東区・浦安市のデザイン分譲住宅｜株式会社ハウセット';
    if(empty($desPage)) $desPage = 'ハウセットは墨田区、江戸川区、江東区、葛飾区、千葉県の浦安市を中心にデザイン分譲住宅、デザイナー新築一戸建てを手がけています。高いデザイン性、暮らしやすい間取り、最新で使いやすい住宅設備、高い断熱性と耐震性。ハウセットの手がける高品質の分譲住宅をぜひご覧ください。';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = '';
  break;
  case 'blog':
    if(empty($titlepage)) $titlepage = 'ハウセットブログ | 江戸川区・江東区・浦安市のデザイン分譲住宅会社';
    if(empty($desPage)) $desPage = '江戸川区・江東区・浦安市でデザイン分譲住宅と注文住宅の販売、施工を行なっているハウセットのブログです。建築についてのノウハウも語っていますのでぜひご覧ください！';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = '';
  break;
  case 'single-blog':
    if(empty($titlepage)) $titlepage = $title_sg.'| 江戸川区・江東区・浦安市のデザイン分譲住宅会社';
    if(empty($desPage)) $desPage = '江戸川区・江東区・浦安市でデザイン分譲住宅と注文住宅の販売、施工を行なっているハウセットのブログです。建築についてのノウハウも語っていますのでぜひご覧ください！';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = '';
  break;
  case 'contact':
    if(empty($titlepage)) $titlepage = 'お問い合わせ | 江戸川区・江東区・浦安市のデザイン分譲住宅はハウセット';
    if(empty($desPage)) $desPage = '墨田区・江戸川区・江東区・千葉県浦安市で高品質な分譲住宅を探すならハウセットにお問い合わせください。';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = '';
  break;
  default:
    if(empty($titlepage)) $titlepage = 'Default';
    if(empty($desPage)) $desPage = '';
    if(empty($keyPage)) $keyPage = '';
    if(empty($txtH1)) $txtH1 = 'H1 Default';
}