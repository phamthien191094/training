<aside class="side">

  <!-- .side__sec01 -->
  <?php
  $args = array(
    'post_type'                => 'blog',
    'orderby'                  => 'id',
    'order'                    => 'desc',
    'hide_empty'               => 1,
    'taxonomy'                 => 'blogcat',
    'pad_counts'               => false
  );
  $categories = get_categories( $args );
  if($categories) {
  ?>
  <div class="side__sec01">
    <p class="side__title side__title-sp active">CATEGORIES</p>
    <ul class="side__list01">
      <?php foreach ($categories as $cat) { ?>
      <li><a href="<?php echo get_term_link($cat->slug,'blogcat');?>"><?php  echo $cat->name; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
  <!-- /.side__sec01 -->

  <!-- .side__sec02 -->
  <?php
  $param_side = array(
    'post_type'                => 'blog',
    'order'                    => 'DESC',
    'post_status'              => 'publish',
    'posts_per_page'           => '5'
  );
  $wp_query_side = new WP_Query($param_side);
  if($wp_query_side->have_posts()):
  ?>
  <div class="side__sec02">
    <p class="side__title">RECENT POST</p>
    <ul class="side__list02">
      <?php while($wp_query_side->have_posts()) : $wp_query_side->the_post(); ?>
      <li>
        <a href="<?php the_permalink(); ?>">
          <?php
          $image = wp_get_attachment_image_src(get_field('main-img'),'thumbnail');
          if ($image) :
            $url_image = $image[0];
          else :
            $url_image = APP_ASSETS."img/common/other/img_nophoto.jpg";
          endif;
          ?>
          <div class="side__list02-img" style="background-image: url(<?php echo $url_image; ?>);"></div>
          <div class="side__list02-info">
            <p class="side__list02-top">
              <span class="side__list02-date"><?php the_time('Y.m.d'); ?></span>
              <?php
              $terms_side = get_the_terms($post->ID, 'blogcat');
              if($terms_side) {
                foreach($terms_side as $term_side) {
              ?>
                <span class="side__list02-cat"><?php echo $term_side->name; ?></span>
              <?php
                }
              }
              ?>
          </p>
            <p class="side__list02-text"><?php the_title(); ?></p>
          </div>
        </a>
      </li>
      <?php endwhile; ?>
    </ul>
  </div>
  <?php endif; wp_reset_postdata();?>
  <!-- /.side__sec02 -->

  <!-- .side__sec03 -->
  <div class="side__sec03">
    <p class="side__title side__title-sp">ARCHIVE</p>
    <ul class="side__list03">
      <?php
      $post_type = array(
        'post_type'                => 'blog',
        'home_url'                 => '',
        'have_count'               => true,
        'add_zero_in_month'        => false,
        'add_zero_in_count'        => false
      );
      echo wp_post_type_archive($post_type);
      ?>
    </ul>
  </div>
  <!-- /.side__sec03 -->
</aside>