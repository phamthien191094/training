<ul class="list01">
  <li>
    <?php
    $prevPost = get_previous_post();
    if($prevPost) {
      $args01 = array(
        'p'                        => $prevPost->ID,
        'post_type'                => 'blog',
        'posts_per_page'           => 1
      );
      $wp_query01 = new WP_Query($args01);
      while($wp_query01->have_posts()) : $wp_query01->the_post();
      ?>
        <a class="list01__btn list01__btn--prev" href="<?php the_permalink(); ?>">PREV</a>
        <a class="list01__item" href="<?php the_permalink(); ?>">
          <?php
          $image = wp_get_attachment_image_src(get_field('main-img'),'medium');
          if ($image) :
            $url_image = $image[0];
          else :
            $url_image = APP_ASSETS."img/common/other/img_nophoto.jpg";
          endif;
          ?>
          <div class="list01__img" style="background-image: url(<?php echo $url_image; ?>);"></div>
          <div class="list01__info">
            <span class="list01__date"><?php the_time('Y.m.d'); ?></span>
            <?php
            $terms_side01 = get_the_terms($prevPost->ID, 'blogcat');
            if($terms_side01) {
              foreach($terms_side01 as $term_side01) {
            ?>
              <span class="list01__cat"><?php echo $term_side01->name; ?></span>
            <?php
              }
            }
            ?>
            <p class="list01__txt"><?php the_title(); ?></p>
          </div>
        </a>
      <?php
      endwhile;
      wp_reset_postdata();
    }
    ?>
  </li>
  <li>
    <?php
    $nextPost = get_next_post();
    if($nextPost) {
      $args02 = array(
        'p'                        => $nextPost->ID,
        'post_type'                => 'blog',
        'posts_per_page'           => 1
      );
      $wp_query02 = new WP_Query($args02);
      while($wp_query02->have_posts()) : $wp_query02->the_post();
      ?>
        <a class="list01__btn list01__btn--next" href="<?php the_permalink(); ?>">NEXT</a>
        <a class="list01__item" href="<?php the_permalink(); ?>">
          <?php
          $image = wp_get_attachment_image_src(get_field('main-img'),'medium');
          if ($image) :
            $url_image = $image[0];
          else :
            $url_image = APP_ASSETS."img/common/other/img_nophoto.jpg";
          endif;
          ?>
          <div class="list01__img" style="background-image: url(<?php echo $url_image; ?>);"></div>
          <div class="list01__info">
            <span class="list01__date"><?php the_time('Y.m.d'); ?></span>
            <?php
            $terms_side02 = get_the_terms($nextPost->ID, 'blogcat');
            if($terms_side02) {
              foreach($terms_side02 as $term_side02) {
            ?>
              <span class="list01__cat"><?php echo $term_side02->name; ?></span>
            <?php
              }
            }
            ?>
            <p class="list01__txt"><?php the_title(); ?></p>
          </div>
        </a>
      <?php
      endwhile;
      wp_reset_postdata();
    }
    ?>
  </li>
</ul>