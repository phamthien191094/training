/* headerMod */
$(window).on('scroll load', function(){
  if($(window).scrollTop() > 60) $("body").addClass('fixHeader');
  else $("body").removeClass('fixHeader');
});


/* for gNavi PC */
$(window).resize(function(){
  $(".navSub").css('display','none');
  $(".gNavi .hasSub").removeClass('active');
});

function gNaviHover() {
  var btn = $(".gNavi .hasSub");
  var submenu = $(".navSub");
  $(btn).hover(function() {
    var shownav = $(this).find(".navSub");
    browserWidth = $(window).width();
    if (browserWidth > 767) {
      if($(shownav).css("display") == "none") {
        $(shownav).stop().slideDown(200);
        $(this).addClass('active');
      }else{
        $(shownav).stop().slideUp(0);
        $(this).removeClass('active');
      }
    }
  },
  function() {
    var shownav = $(this).find(".navSub");
    browserWidth = $(window).width();
    if (browserWidth > 767) {
      $(shownav).stop().slideUp(0);
      $(this).removeClass('active');
    }
  });
}

gNaviHover();

$('.closeSub').click(function(){
  $(this).parent(".navSub").stop().slideUp(200);
  $(this).parents(".hasSub").removeClass('active');
});
/* end gNavi PC */


/* menu header SP */
$('.hamberger').click(function(){
  $(this).toggleClass("active");
  $("body").toggleClass("layerOn");
});

$('.close_layer, .gNavi li a').click(function(){
  $('.hamberger').removeClass("active");
  $("body").toggleClass("layerOn");
});

$('.gNavi .hasSub .plus').click(function(){
  $(this).parent('.hasSub').toggleClass("active");
  $(this).next('.navSub').stop().slideToggle(200);
});

/* ======================================
scroll header
====================================== */
$(window).scroll(function () {
  if ($(this).scrollTop() > 20) {
    $('.header').addClass('scroll');
  } else {
    $('.header').removeClass('scroll');
  }
}).scroll();

/* ======================================
body fix
====================================== */
var scrollPosi;

function bodyFix() {
  scrollPosi = $(window).scrollTop();
  $('body').css({
    'position': 'fixed',
    'width': '100%',
    'z-index': '1',
    'top': -scrollPosi
  });
}

function bodyFixReset() {
  $('body').css({
    'position': 'relative',
    'width': 'auto',
    'top': 'auto'
  });
  $('body,html').scrollTop(scrollPosi);
}

/* ======================================
menu sp
====================================== */
$(".header__open").on("click", function () {
  $(this).toggleClass('active');
  $(".header").toggleClass('bg');
  $(".menu-sp").slideToggle(600);
  if ($(this).hasClass('active')) {
    bodyFix();
  } else {
    bodyFixReset();
  }
});
// subNav menu sp
$(".js-slide").on("click", function (e) {
  if ($(window).width() < 768) {
    $(this).toggleClass('active');
    $(this).siblings().slideToggle(400);
    e.preventDefault();
  }
});
//  close menu sp
$(".menu-sp__close").on("click", function () {
  $(".menu-sp").slideUp(600);
  $(".header__open").removeClass('active');
  $(".header").removeClass('bg');
  bodyFixReset();
});

/* ======================================
scroll top
====================================== */
$(window).scroll(function () {
  if ($(this).scrollTop() > 200) {
    $(".footer__scrolltop").fadeIn();
  } else {
    $(".footer__scrolltop").fadeOut();
  }
}).scroll();
$(".footer__scrolltop").on("click", function () {
  $('body,html').animate({scrollTop: 0}, 500);
  return false;
});

/* ======================================
slideToggle side
====================================== */
$(".side__list03-item").on("click", function () {
  $(this).toggleClass('active');
  $(this).siblings().slideToggle();
});
// sp
$(".side__title-sp").each(function() {
  if ($(window).width() < 768) {
    if ($(this).hasClass('active')) {
      $(this).siblings().show();
    } else {
      $(this).siblings().hide();
    }
  }
});
$(".side__title-sp").on("click", function () {
  if ($(window).width() < 768) {
    $(this).toggleClass('active');
    $(this).siblings().slideToggle();
  }
});