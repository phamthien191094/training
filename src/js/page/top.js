$('.slider').slick({
  dots: true,
  infinite: true,
  speed: 1500,
  fade: true,
  autoplay: true,
  autoplaySpeed: 3500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  focusOnSelect: false,
  pauseOnHover: false,
  accessibility: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        dots: false
      }
    }
  ]
});

$(".sec01__scroll-down").on("click", function () {
  var toId = $(this).attr('href');
  $('body,html').animate({scrollTop: $(toId).offset().top - 114}, 300);
  return false;
});